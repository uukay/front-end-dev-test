var promise = new Promise(function(resolve, reject) {
  if (true) {
    resolve("Stuff worked!");
  } else {
    reject(Error("It broke"));
  }
});

const run = () => {
  return promise;
};

const handler = data => {
  console.log(`${data} in handler`);
};

const runAnotherFunction = data => {
  console.log(`${data} in runAnotherFunction`);
  return promise;
};

// E.g. Puzzle 4
run()
  .then(runAnotherFunction)
  .then(handler);

const puzzle_1 = () => {
  console.log(`run
		|-----------------|
											runAnotherFunction(undefined)
											|------------------|
																				 handler(resultOfrunAnotherFunction)
																				 |------------------|`);
};

const puzzle_2 = () => {
  console.log(`run
	|-----------------|
										runAnotherFunction(undefined)
										|------------------|
																			 handler(undefined)
																			 |------------------|`);
};

const puzzle_3 = () => {
  console.log(`run
	|-----------------|
										runAnotherFunction(undefined)
										|------------------|
																			 handler(resultOfrunAnotherFunction)
																			 |------------------|`);
};

const puzzle_4 = () => {
  console.log(`run
	|-----------------|
										runAnotherFunction(resultOfrun)
										|------------------|
																			 handler(resultOfrunAnotherFunction)
																			 |------------------|`);
};
