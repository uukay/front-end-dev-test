#Front End Dev Test #1 for Audience Republic

Hi Jason, to run my quick challenge response, simply run $npm run quick-challenge and the expected result is returned and console logged. Students.json is treated as an external file in the modules folder, and init is the single entry point for my logic.

To check my "Puzzle Game" responses, see inside "puzzle-game/init.js" where you'll see I've set up some examples to test the 4 puzzle scenarios and I've stored the answers to the 4 puzzles inside of functions. You can do $npm run puzzle-game to run the test for the particular case that is set up in the file.

Cheers,

Will