const fs = require("fs");
const students_json = JSON.parse(
  fs.readFileSync(`${__dirname}/modules/students.json`, "utf-8")
);
const students = students_json.students;

// Using the students array below, write a javascript function to return an object containing:
// The name of the class as the key.
// The total attended lessons for each class.
// The average amount of attended lessons for each class.

function countStats(classes, students) {
  var class_stats = Object.assign(classes);

  for (let _class in classes) {
    let single_class_arr = students.filter(student => student.class === _class);
    let total = single_class_arr.reduce((acc, currVal) => {
      return acc + currVal["attended"];
    }, 0);
    let average = Math.floor(total / single_class_arr.length);
    class_stats[_class]["total"] = total;
    class_stats[_class]["average"] = average;
  }

  return class_stats;
}

function getClasses(students) {
  var classes = {};
  for (let i = 0; i < students.length; i++) {
    let students_class = students[i]["class"];
    if (!classes.hasOwnProperty(students_class)) {
      classes[students_class] = {};
    }
  }
  return classes;
}

function classStats(students) {
  var classes = getClasses(students);
  var class_stats = countStats(classes, students);

  console.log(class_stats);
}

classStats(students);
